<?php

declare(strict_types=1);

namespace FlyingAnvil\RetroTool\Converter\Image\Snes\DataObject;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\Libfa\DataObject\Exception\ValueException;
use ReflectionClass;
use ReflectionClassConstant;
use Stringable;

final class SnesGfxMode implements DataObject, Stringable
{
    public const MODE_2BPP = '2bpp';
    public const MODE_3BPP = '3bpp';
    public const MODE_4BPP = '4bpp';
    public const MODE_8BPP = '8bpp';

    public function __construct(private string $mode)
    {
        if (!in_array($mode, self::getValidValues(), true)) {
            throw new ValueException(sprintf(
                'Mode "%s" is not valid',
                $mode,
            ));
        }
    }

    public static function create(string $mode): self
    {
        return new self($mode);
    }

    public static function getValidValues(): array
    {
        $reflection = new ReflectionClass(self::class);
        return $reflection->getConstants(ReflectionClassConstant::IS_PUBLIC);
    }

    public function getSizePerTile(): int
    {
        return match ($this->mode) {
            self::MODE_2BPP => 2 * 8,
            self::MODE_3BPP => 3 * 8,
            self::MODE_4BPP => 4 * 8,
            self::MODE_8BPP => 8 * 8,
        };
    }

    public function jsonSerialize()
    {
        return $this->mode;
    }

    public function __toString(): string
    {
        return $this->mode;
    }
}
