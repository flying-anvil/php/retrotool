<?php

declare(strict_types=1);

namespace FlyingAnvil\RetroTool\Converter\Image\Snes;

use FlyingAnvil\RetroTool\Converter\Image\Snes\DataObject\SnesGfxMode;
use FlyingAnvil\Libfa\Image\Drawing\Tile\Tile;
use FlyingAnvil\Libfa\Image\Drawing\Tile\Tiles;
use FlyingAnvil\Libfa\Wrapper\File;

class GfxTilesetParser
{
    public function convert(File $file, ?SnesGfxMode $mode = null): Tiles
    {
        if (!$mode) {
            $mode = SnesGfxMode::create(SnesGfxMode::MODE_4BPP);
        }

        if ((string)$mode !== SnesGfxMode::MODE_4BPP) {
            throw new \Exception('only 4bpp is currently supported');
        }

        $file->open();

        $sizePerTile = $mode->getSizePerTile();
        $tilesInFile = $file->getFileSize() / $sizePerTile;
        $tiles       = Tiles::createEmpty();

        for ($i = 0; $i < $tilesInFile; $i++) {
            $file->seek($i * $sizePerTile);

            $tile = Tile::create(8, 8);

            // Read 8 rows (tiles are 8x8)
            for ($y = 0; $y < 8; $y++) {
                // Read two planar bitplanes
                $bitplane1 = $file->readUnsignedByte();
                $bitplane2 = $file->readUnsignedByte();

                // Skip to intertwined offset (16 bytes per two planar bitplanes - 2 already read)
                $file->seek(14, SEEK_CUR);

                // Read two planar bitplanes
                $bitplane3 = $file->readUnsignedByte();
                $bitplane4 = $file->readUnsignedByte();

                // Rewind to the beginning of the next two planar bitplanes ()
                $file->seek(-16, SEEK_CUR);

                for ($x = 7; $x >= 0; $x--) {
                    $mask = 1 << $x;
                    $bit1 = ($bitplane1 & $mask) >> $x;
                    $bit2 = ($bitplane2 & $mask) >> $x;
                    $bit3 = ($bitplane3 & $mask) >> $x;
                    $bit4 = ($bitplane4 & $mask) >> $x;

                    $combined = $bit1 | ($bit2 << 1) | ($bit3 << 2) | ($bit4 << 3);
                    $tile->setPixel(7 - $x, $y, $combined);
                }
            }

            $tiles->addTile($tile);
        }

        return $tiles;
    }
}
