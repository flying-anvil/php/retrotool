<?php

use FlyingAnvil\RetroTool\Converter\Image\Snes\GfxTilesetParser;
use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\IndexedColorPalettes;
use FlyingAnvil\Libfa\Image\Drawing\Tile\TileMap;
use FlyingAnvil\Libfa\Wrapper\File;

require_once __DIR__ . '/../vendor/autoload.php';

if ($argc <= 3 && $argc >= 4) {
    fwrite(STDERR, 'Usage: ' . $argv[0] . ' path/to/gfx.bin path/to/palette.pal $paletteIndex' . PHP_EOL);
    fwrite(STDERR, 'If $paletteIndex is missing, use a random one' . PHP_EOL);
    exit(1);
}

$gfx = File::load($argv[1]);
$pal = File::load($argv[2]);

$palette      = IndexedColorPalettes::loadFromPalFile($pal, 16);
$paletteIndex = (int) ($argv[3] ?? random_int(0, $palette->count() - 1));

$converter = new GfxTilesetParser();
$tiles     = $converter->convert($gfx);

try {
    $tileMap = TileMap::createFromTilesGuessSize(
        $tiles,
        8, 8,
    );
} catch (Exception $exception) {
    echo $exception->getMessage(), PHP_EOL, PHP_EOL;

    $tileMap = TileMap::createFromTiles(
        $tiles,
        16, ceil($tiles->count() / 16),
        8, 8,
    );
}

$tileMap->drawSmall($palette->getPaletteByIndex($paletteIndex), true);

echo PHP_EOL;
